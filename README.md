# Marxist Tamil

Android app to read & listen to essays from the political and philosophical monthly Tamil Magazine, [Marxist](https://marxistreader.app/).

Note: This is a fork of [Marxist Reader](https://github.com/khaleeljageer/MarxistReader) app developed by TNCPIM's multimedia team. The objective of this fork is to maintain a F-droid compatible version of the Marxist Reader app free from tracking and analytics.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/io.gitlab.pycpim.marxist/)

Or download the latest APK from the [Releases Section](https://gitlab.com/pycpim/marxist-tamil/-/releases).

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

Check [LICENSE](LICENSE) for more details
