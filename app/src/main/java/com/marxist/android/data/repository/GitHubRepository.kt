package com.marxist.android.data.repository

import com.marxist.android.data.api.GitHubHelper
import com.marxist.android.model.BooksResponse
import com.marxist.android.model.QuotesResponse
import com.marxist.android.utils.network.NetworkResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GitHubRepository @Inject constructor(private val gitHubHelper: GitHubHelper) {

    suspend fun getBooks(): Flow<NetworkResponse<BooksResponse>> = flow {
        try {
            val response = gitHubHelper.getBooks()
            if (response.isSuccessful) {
                val list: BooksResponse? = response.body()
                if (list != null && list.books.isNotEmpty()) {
                    emit(NetworkResponse.Success(list))
                } else {
                    emit(NetworkResponse.EmptyResponse)
                }
            } else {
                emit(NetworkResponse.Error(response.message() ?: "Unknown error"))
            }
        } catch (exception: Exception) {
            Timber.e(exception.message);
            emit(NetworkResponse.Error("Unable to fetch books due to network issue!"));
        }
    }

    suspend fun getQuotes(): Flow<NetworkResponse<QuotesResponse>> = flow {
        try {
            val response = gitHubHelper.getQuotes()
            if (response.isSuccessful) {
                val list: QuotesResponse? = response.body()
                if (list != null && list.quotes.isNotEmpty()) {
                    emit(NetworkResponse.Success(list))
                } else {
                    emit(NetworkResponse.EmptyResponse)
                }
            } else {
                emit(NetworkResponse.Error(response.message() ?: "Unknown error"))
            }
        } catch (exception: Exception) {
            emit(NetworkResponse.Error("Unable to fetch quotes due to network issue!"));
        }
    }
}