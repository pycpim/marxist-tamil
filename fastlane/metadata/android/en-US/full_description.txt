Marxist Tamil is an android app to read and listen to detailed political & philosophical essays, from monthly Tamil magazine called Marxist, published by TNCPIM.

* Read, Listen & Share essays.
* Get Marxist quotes in the app.
* Read DRM free Ebooks on Communism.

Note: This is a modified version of the original Marxist Reader app available in Playstore. Tracking, Analysis & Non-free network service related dependencies are removed, so that readers can be sure that the app doesn't violate their privacy through analytics & trackers.
